import { Chart, ChartData, ChartOptions, registerables } from "chart.js"
import React, { useEffect, useState } from "react";
import { useRef } from "react";
import "./GaugeChart.css"

export interface GaugeChartProps {
    color: {
        red: number,
        green: number,
        blue: number,
        alpha?: number
    },
    label: string,
    percent: number,
    value: number
}

const GaugeChart = ({
    color,
    label,
    percent,
    value
} : GaugeChartProps) => {

    const canvasRef = useRef<HTMLCanvasElement>(null)
    const [chartJsRef, setChartJsRef] = useState<Chart | null>(null)
    
    Chart.register(...registerables)

    async function createChart() {
        if (canvasRef.current === null) return;

        if (chartJsRef !== null) {
            chartJsRef.destroy()
        }

        const chart = new Chart(canvasRef.current, {
            type: "doughnut",
            data: {
                labels: [label],
                datasets: [{
                    data: [
                        percent,
                        100 - percent,
                    ],
                    backgroundColor: [`rgba(${color.red}, ${color.green}, ${color.blue}, ${color.alpha ? color.alpha : 0.7})`, "rgb(62, 62, 62)"],
                    borderWidth: 0
                }]
            },
            options: {
                responsive: true,
                cutout: "90%",
                events: [],
                plugins: {
                    legend: {
                        display: false
                    }
                },
                animation: false
            }
        })
        await setChartJsRef(chart);
    }

    useEffect(() => {
        createChart()
    }, [color, label, percent, value])

    let classes = ["chart-gauge"]

    return (
        <div className={classes.join(" ")}>
            <div>
                <canvas ref={canvasRef} />
                <p className="value">{value}</p>
            </div>
            <p className="label">{label}</p>
            <p className="percent">{percent}%</p>
        </div>
    )
}

export default GaugeChart;