import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import GaugeChart, { GaugeChartProps } from "./GaugeChart";

export default {
  title: "Components/Chart/GaugeChart",
  component: GaugeChart,
} as Meta;

const Template: Story<GaugeChartProps> = (args) => <GaugeChart {...args}></GaugeChart>;


export const Default = Template.bind({});
Default.args = { 
    color: {
        red: 28,
        green: 233,
        blue: 185,
      },
    label: "Accomplissement",
    percent: 64,
    value: 150
}