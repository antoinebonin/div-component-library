import React from "react";
import "./Label.css"

export interface LabelProps {
    elementLink?: string;
    value: string;
    isError?: boolean;
    requierd?: boolean;
}

const Label = ({
    value = "Adresse mail :",
    elementLink = "email",
    isError = false,
    requierd = false
}: LabelProps) => {

    let classes : string[] = ["label"];
    (isError) ? classes.push('label--error') : null;

    return (
        <label 
        className={classes.join(" ")}
        htmlFor={elementLink}>
            {value} {requierd ? <span>*</span> : ""}
        </label>
    )
}

export default Label;