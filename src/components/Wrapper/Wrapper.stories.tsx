import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import Wrapper, { WrapperProps } from "./Wrapper";

export default {
  title: "Components/Layout/Wrapper",
  component: Wrapper,
  argTypes: {},
} as Meta;

// Create a master template for mapping args to render the Button component
const Template: Story<WrapperProps> = (args) => <Wrapper {...args}> <h1>Titre test</h1> <p>Texte placeholder</p> <img src="https://via.placeholder.com/150" /> </Wrapper>;

// Reuse that template for creating different stories
export const Small = Template.bind({});
Small.args = { size: "small" }

// Reuse that template for creating different stories
export const Medium = Template.bind({});
Medium.args = { size: "medium" }

// Reuse that template for creating different stories
export const Large = Template.bind({});
Large.args = { size: "large" }