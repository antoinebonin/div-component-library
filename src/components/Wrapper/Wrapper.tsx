import React, { ReactChildren, ReactNode } from "react";
import "./wrapper.css";

export interface WrapperProps {
  children:ReactNode;
  size?: "small" | "medium" | "large";
};

/**
 * Primary UI component for user interaction
 */
const Wrapper = ({
  children,
  size = "medium",
}: WrapperProps) => {
  return (
    <div className={["wrapper", `wrapper--${size}`].join(" ")}
    >
      {children}
    </div>
  );
};

export default Wrapper;
