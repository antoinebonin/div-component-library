import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import CreditCard, { CreditCardProps } from "./CreditCard";

export default {
  title: "Components/Money/CreditCard",
  component: CreditCard,
} as Meta;

const Template: Story<CreditCardProps> = (args) => <CreditCard {...args}></CreditCard>;


export const Default = Template.bind({});
Default.args = {
    accountNumber: "9999 9999 9999 9999",
    expirationDate: new Date(),
    solde: 1400.40
}