import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import Loader , { LoaderProps } from "./Loader";

export default {
    title: "Components/Loader",
    component: Loader
} as Meta;

// Create a master template for mapping args to render the Button component
const Template: Story<LoaderProps> = (args) => <Loader {...args} />;

// Reuse that template for creating different stories
export const Default = Template.bind({});

export const Larger = Template.bind({});
Larger.args = { ...Default.args, size : 2, borderSize: 4 };
