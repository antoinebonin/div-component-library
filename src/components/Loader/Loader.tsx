import React from "react";
import './loader.css';

export interface LoaderProps {
    /**
     * width and height in rem
     */
    size?: number;
    /**
     * border size in px
     */
    borderSize?: number;
    /**
     * variant border color
     */
    variant: "success" | "error" | "primary" | "secondary";
}

const Loader = ({size = 1, borderSize = 2, variant = "primary"}: LoaderProps) => {
    return (
        <div style={{
            width: `${size}rem`,
            height: `${size}rem`,
            border: `solid ${borderSize}px #ebebeb`,
            borderTop: `solid ${borderSize}px var(--${variant})`
        }} className="storybook-loader"/>
    );
};

export default Loader;
