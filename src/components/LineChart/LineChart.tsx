import { Chart, ChartData, ChartOptions, registerables } from "chart.js"
import React, { useEffect, useState } from "react";
import { useRef } from "react";
import "./LineChart.css"

export interface LineChartProps {
    dataset: ChartData;
    options: ChartOptions;
    color: {
        red: number,
        green: number,
        blue: number,
        alpha?: number
    }
}

const LineChart = ({
    dataset,
    options,
    color
} : LineChartProps) => {

    const canvasRef = useRef<HTMLCanvasElement>(null)
    const [chartJsRef, setChartJsRef] = useState<Chart | null>(null)
    
    Chart.register(...registerables)

    async function createChart() {
      if (canvasRef.current === null) return;

        const chart = (chartJsRef === null) ? 
            new Chart(canvasRef.current, {
                type: "line",
                data: dataset,
                options: options
            }) : chartJsRef;

        if (chart !== null) {
            chart.data = dataset
        }
    
      const ctx = canvasRef.current.getContext("2d")
      const height = canvasRef.current.height
      if (ctx) {
          let gradient = ctx?.createLinearGradient(0, 0, 0, height/2)
          gradient.addColorStop(0, `rgba(${color.red}, ${color.green}, ${color.blue}, ${color.alpha ? color.alpha : 0.7})`);
          gradient.addColorStop(0.85, `rgba(${color.red}, ${color.green}, ${color.blue}, 0)`);
          gradient.addColorStop(1, `rgba(${color.red}, ${color.green}, ${color.blue}, 0)`);
          dataset.datasets[0].backgroundColor = gradient
          chart.update()
      }
    
      await setChartJsRef(chart)
    }

    useEffect(() => {
        createChart()
    }, [dataset])

    return (
        <canvas
        ref={canvasRef}>

        </canvas>
    )
}

export default LineChart;