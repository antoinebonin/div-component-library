import React from "react"
import "./Input.css"


export interface InputProps {
    name?: string;
    type?: "email" | "text" | "password" | "search" | "url" | "number"
    size?: "small" | "medium" | "large";
    placeholder?: string;
    value?: string;
    onChange: (
        event: React.FormEvent<HTMLInputElement>
    ) => void;
    isError?: boolean;
    requierd?: boolean;
}

const Input = ({
    name = "email",
    type = "text",
    size = "medium",
    placeholder = "Votre adresse email",
    onChange,
    isError = false,
    requierd = false,
    value
}: InputProps) => {

    let classes : string[] = ["input", `input--${size}`];
    (isError) ? classes.push('input--error') : null;

    return (
        <input
            name={name}
            type={type}
            className={classes.join(" ")}
            placeholder={placeholder}
            required={requierd}
            value={value}
            size={placeholder ? placeholder.length : 18}
            onChange={(e) => onChange(e)}
            >
        </input>
    )
}

export default Input;