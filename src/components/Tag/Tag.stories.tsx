import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import Tag, { TagProps } from "./Tag";

export default {
  title: "Components/Texts/Tag",
  component: Tag,
} as Meta;

const Template: Story<TagProps> = (args) => <Tag {...args}/>;

export const Books = Template.bind({});
Books.args = { value: "Books", color: "#ff0000" };

export const Greens = Template.bind({});
Greens.args = { value: "Greens", color: "#00ff00" };