import React, { ReactNode } from "react";
import "./table.css";

export interface TableProps {
  children:ReactNode;
};

/**
 * Primary UI component for user interaction
 */
const Table = ({
  children
}: TableProps) => {
  return (
    <table className={["table"].join(" ")}>
      {children}
    </table>
  );
};

export default Table;
