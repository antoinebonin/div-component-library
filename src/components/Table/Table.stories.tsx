import React from "react";
import { Meta } from "@storybook/react/types-6-0";
import { Story } from "@storybook/react";
import Table, { TableProps } from "./Table";
import Wrapper from "../Wrapper/Wrapper";

export default {
  title: "Components/Table",
  component: Table
} as Meta;

const Template: Story<TableProps> = (args) =>
<Wrapper size={'medium'}>
  <Table {...args}>
    <thead>
        <tr>
            <th>hello</th>
            <th>hello</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>world</td>
            <td>world</td>
        </tr>
        <tr>
            <td>world</td>
            <td>world</td>
        </tr>
    </tbody>
  </Table>
</Wrapper>;

export const Default = Template.bind({});
Default.args = {  };